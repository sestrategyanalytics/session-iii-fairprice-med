-- 10 segments
drop table if exists #FP2018rank;
select csn, sum(est_gross_transaction_amt) as spend, sum(transcount) as transactions, row_number() over(order by spend desc) as rank, rank/124249 as decile
into #FP2018rank
from link.view_facttrans_plus
where corporation_id in ('FP','101770000000')
and pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and yearid=2018 and est_gross_transaction_amt>0
group by csn
order by spend desc;

select decile, count(csn), sum(spend), avg(spend), avg(transactions/1.0) from #FP2018rank
group by decile;

-- CSN in top decile
with #FP2018rank as
(select csn, sum(est_gross_transaction_amt) as spend, sum(transcount) as transactions, row_number() over(order by spend desc) as rank, rank/124249 as decile
from link.view_facttrans_plus
where corporation_id in ('FP','101770000000')
and pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and yearid=2018 and est_gross_transaction_amt>0
group by csn)

select a.csn, active, membership_type, acquisition_channel, gender_code, marital_status, dob, datediff(year, dob, getdate()) as age, postal_code, race_code, segment_desc,
case when comms_by_sms_flag ='Y' AND valid_sms ='Y' then 'Y' else 'N' end as SMS, case when (comms_by_email_flag = 'Y' and valid_email = 'Y' and hard_bounce = 'N') then 'Y' else 'N' end as EDM
from #FP2018rank a
join link.link_client_master b on a.csn=b.csn
left join lac.client_segmentation_cluster d on a.csn=d.csn
left join lac.client_segmentation_cluster_desc e on d.cluster=e.segment;

-- Tableau
drop table if exists LAC.Manf_FPloyaltytiers;
with #FPtiers as
(select csn, sum(est_gross_transaction_amt) as spend, sum(transcount) as transactions, row_number() over(order by spend desc) as rank, rank/62125 as tier
from link.view_facttrans_plus
where corporation_id in ('FP','101770000000')
and pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and yearid=2018 and est_gross_transaction_amt>0
group by csn)

select tier, a.csn, case when transactions>0 then transactions else 1 end as transactioncount, spend/transactioncount as basketsize, active, membership_type, acquisition_channel, gender_code, marital_status, dob, datediff(year, dob, getdate()) as age, postal_code, race_code, segment_desc,
case when comms_by_sms_flag ='Y' AND valid_sms ='Y' then 'Y' else 'N' end as SMS, case when (comms_by_email_flag = 'Y' and valid_email = 'Y' and hard_bounce = 'N') then 'Y' else 'N' end as EDM
into LAC.Manf_FPloyaltytiers
from #FPtiers a
join link.link_client_master b on a.csn=b.csn
left join lac.client_segmentation_cluster d on a.csn=d.csn
left join lac.client_segmentation_cluster_desc e on d.cluster=e.segment;

select top 100 * from LAC.Manf_FPloyaltytiers;


-- 100 segments
drop table if exists #FP2018;
select csn, sum(est_gross_transaction_amt) as spend, sum(transcount) as transactions
into #FP2018
from link.view_facttrans_plus
where corporation_id in ('FP','101770000000')
and pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and yearid=2018 and est_gross_transaction_amt>0
group by csn
having spend>10000;

select count(csn), sum(spend) from #FP2018;

-- top 1%
drop table if exists LAC.Manf_FPloyaltytierstop1;
with #FPtiers as
(select csn, sum(est_gross_transaction_amt) as spend, sum(transcount) as transactions, row_number() over(order by spend asc) as rank, rank/12425 as tier
from link.view_facttrans_plus
where corporation_id in ('FP','101770000000')
and pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and yearid=2018 and est_gross_transaction_amt>0
group by csn)

select tier+1 as percentile, a.csn, spend, case when transactions>0 then transactions else 1 end as transactioncount, spend/transactioncount as basketsize, active, membership_type, acquisition_channel, gender_code, marital_status, dob, datediff(year, dob, getdate()) as age, postal_code, race_code, segment_desc,
case when comms_by_sms_flag ='Y' AND valid_sms ='Y' then 'Y' else 'N' end as SMS, case when (comms_by_email_flag = 'Y' and valid_email = 'Y' and hard_bounce = 'N') then 'Y' else 'N' end as EDM
into LAC.Manf_FPloyaltytierstop1
from #FPtiers a
join link.link_client_master b on a.csn=b.csn
left join lac.client_segmentation_cluster d on a.csn=d.csn
left join lac.client_segmentation_cluster_desc e on d.cluster=e.segment
where tier=99;

select top 100 * from LAC.Manf_FPloyaltytierstop1;

with #tiers as
(select csn, sum(est_gross_transaction_amt) as spend, sum(transcount) as transactions, row_number() over(order by spend asc) as rank, rank/12425 as tier
from link.view_facttrans_plus
where corporation_id in ('FP','101770000000')
and pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and yearid=2018 and est_gross_transaction_amt>0
group by csn)

select count(csn), sum(spend), tier from #tiers group by tier;
