# Session III: FairPrice + MED

## FairPrice
Here is a couple of dashboards created at the request of William Chung, to show the spending behaviour and demographics of FairPrice customers, segmented into spending tiers.

## MED
We created a dashboard for the Membership Department(MED) of NTUC Union last year, allowing them to track their member acquisition and activation, as well as engagement, at a Union-specific level. When it was first published, they used it extensively to generate reports for each of the Unions, as well as in external presentations.

However, it is important to note that the data has been frozen as of October 2018. During the course of developing this, we discovered many discrepancies in membership status between Link data and MED data. Hence, we did a one-time data sync in October so the numbers would be accurate at that point in time (see link.link_cls_loyalty_account_extension).

Going forward, we would require access to MED data in order to move this dashboard into production.

For now, we can identify Union members through this query in link.link_cls_card:
case
when card_no like '810100%' or card_no like '810800%' then 'NTUC Union'
when card_no like '830100%' then 'NTUC Union Visa'
when card_no like '810300%' then 'nEBO'
else 'Non-Union' end as card_type