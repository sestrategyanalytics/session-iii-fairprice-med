drop table if exists #unionmembers;
with #medlist as
(select distinct b.csn, a.union_name, a.membership_type, min(a.start_date) as joindate
from med.med_membership_union_type a
join link.link_client_master b on a.nric=b.client_id_sha256
join link.link_cls_card c on b.csn=c.csn
where c.card_no like '810100%' or c.card_no like '810800%' or c.card_no like '830100%'
group by b.csn, a.union_name, a.membership_type)
select csn, union_name, membership_type, min(joindate) as joindate
into #unionmembers
from 
(select * from #medlist
union
select distinct a.csn, b.main_union as union_name, b.link_membership_type as membership_type, min(a.activation_date) as joindate
from link.link_cls_card a
join link.link_cls_loyalty_account_extension b on a.csn=b.csn
where a.card_no like '810100%' or a.card_no like '810800%' or a.card_no like '830100%'
and a.csn not in (select csn from #medlist)
and b.main_union is not NULL
and b.main_union in (select union_name from #medlist)
and b.link_membership_type is not NULL
and a.activation_date is not NULL
group by a.csn, union_name, membership_type
having min(a.activation_date) between '2018-10-15' and getdate())
group by csn, union_name, membership_type;

drop table if exists #2018transactions;
select * 
into #2018transactions
from link.view_facttrans_plus
where yearid ='2018'
AND CLUB_CODE = 'LNK' AND POOL_ID = 'P01' AND cancel_ind = 'N' AND pointID in ('I','R');

drop table if exists #clientcleaned;
select distinct client_id_sha256, csn, active, gender_code, marital_status, dob, postal_code, race_code
into #clientcleaned
from link.link_client_master;

drop table if exists lac.manf_union_all_v2;
select distinct A.csn, G.segment_desc, G.explaination, C.transaction_date, D.corporate_name_english_1, E.main_category, C.est_gross_transaction_amt, C.transcount, C.pointid, C.point_earned, A.union_name, A.membership_type, A.joindate, B.active, B.gender_code, B.marital_status, B.dob, B.postal_code, B.race_code, case
when h.card_no like '810100%' or h.card_no like '810800%' then 'NTUC Union'
when h.card_no like '830100%' then 'NTUC Union Visa'
when h.card_no like '810300%' then 'nEBO'
else 'Non-Union' end as card_type
into lac.manf_union_all_v2
from #unionmembers A
join #clientcleaned B on A.csn=B.csn
left join #2018transactions C on A.csn=C.csn
left join link.link_cls_corporation D on C.corporation_id=D.corporate_id
left join lac.temp_table_mapping E on C.corporation_id=E.corporate_id
left join lac.client_segmentation_cluster F on C.csn=F.csn
left join lac.client_segmentation_cluster_desc G on F.cluster=G.segment
left join link.link_cls_card H on C.hcardno=FUNC_SHA256(H.CARD_NO);
